#!/bin/bash

# Correct permissions on volumes
chown -R apache /var/www/html/owncloud/data
chown -R apache /var/www/html/owncloud/config

# TODO: ssl key generating

# TODO: apache config
if [ ! -z "$OWNCLOUD_URL_PATH" ]; then
  read -d '' HTTP_ALIAS <<-EOFALIAS
<IfModule mod_alias.c>
Alias /$OWNCLOUD_URL_PATH /var/www/html/owncloud
</IfModule>
EOFALIAS
fi

echo 'Creating httpd owncloud.conf file ...'
cat > /etc/httpd/conf.d/owncloud.conf <<-EOFHTTP
${HTTP_ALIAS}
<Directory “/var/www/html/owncloud”>
Options Indexes FollowSymLinks
AllowOverride All
Order allow,deny
allow from all
</Directory>
EOFHTTP

# Create php mysql config
echo 'Creating php mysql.ini file ...'
cat > "/etc/php.d/mysql.ini" <<-EOFSQL
extension=pdo_mysql.so
extension=mysql.so

[mysql]
mysql.allow_local_infile=On
mysql.allow_persistent=On
mysql.cache_size=2000
mysql.max_persistent=-1
mysql.max_links=-1
mysql.default_port=
mysql.default_socket=/var/lib/mysql/mysql.sock
mysql.default_host=
mysql.default_user=
mysql.default_password=
mysql.connect_timeout=60
mysql.trace_mode=Off
EOFSQL

# Create php apc config
if [ -z "$PHP_APC_SIZE" ]; then
  PHP_APC_SIZE="256M"
fi

echo 'Creating php apc.ini with apc.shm_size=$PHP_APC_SIZE ...'
cat > "/etc/php.d/apc.ini" <<-EOFAPC
extension=apc.so
apc.enabled=1
apc.ttl=72000
apc.user_ttl=72000
apc.gc_ttl=3600
apc.shm_size=${PHP_APC_SIZE}
apc.stat=1
apc.enable_cli=1
apc.file_update_protection=2
apc.max_file_size=5M
apc.num_files_hint=200000
apc.user_entries_hint=20000
EOFAPC

# Create ownCloud autoconfig.php
echo 'Creating ownCloud autoconfig.php ...'
cat > "/var/www/html/owncloud/config/autoconfig.php" <<-EOFCONFIGPHP
<?php
$AUTOCONFIG = array (
  'directory' => '/var/www/html/owncloud/data',
  'apps_paths' => array (
      0 => array (
              'path'     => OC::$SERVERROOT.'/apps',
              'url'      => '/apps',
              'writable' => false,
      ),
      1 => array (
              'path'     => OC::$SERVERROOT.'/extra_apps',
              'url'      => '/extra_apps',
              'writable' => true,
      ),
  ),
  'appstoreenabled' => true,
  'appstoreurl' => 'http://api.apps.owncloud.com/v1',
  'log_type' => 'owncloud',
  'updatechecker' => true,
  'forcessl' => true,
)
EOFCONFIGPHP

/usr/sbin/httpd -DFOREGROUND -k start
