FROM centos:centos7
MAINTAINER Christian Witt c.witt.1900@gmail.com

RUN echo SELINUX=disabled >> /etc/selinux/config

RUN yum update -y && \
yum install epel-release -y && \
cd /etc/yum.repos.d/ && \
wget http://download.opensuse.org/repositories/isv:ownCloud:community/CentOS_CentOS-7/isv:ownCloud:community.repo && \
yum install owncloud sqlite httpd php-pear php-intl php-ldap php-mysql php-pgsql php-mcrypt php-pecl-imagick -y && \
yum clean all

RUN pecl install apc

EXPOSE 80 80
EXPOSE 443 443

VOLUME /var/www/html/owncloud/data /var/www/html/owncloud/config /var/www/html/owncloud/extra_apps /etc/pki/tls/certs

ADD startup.sh /startup.sh
RUN chmod +x /startup.sh
CMD /startup.sh
